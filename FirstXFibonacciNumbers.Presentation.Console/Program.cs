﻿using General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstXFibonacciNumbers.Presentation.Console
{

    // Get the first X fib numbers..

    public class Program
    {
        static int _firstxfibs = 100;

        static void Main(string[] args)
        {
            var result = GetFibonaccis(_firstxfibs);
            var printableresult = string.Join(", ", result);
            Logger.WriteLine(printableresult);
            
            // Lets just prevent us from exiting..
            System.Console.ReadKey();
        }

        // One way of doing it.. but if solving the problem in an efficient way is the actual problem.. then Google should be your friend in this case.
        // since there are literally hundreds of thousands articles of how to do this even more efficient... 
        // btw.. ulong is your friend too when needed.. ints are more "limited" when we are doing "positive-only" calculations since they overflow quite quickly and then go negative if not checked..

        // Will not bother making this an extension method of System.Math since I consider this to be an "edge case"..
        public static ulong[] GetFibonaccis(int quantity)
        {
            var result = new ulong[quantity];
            for(var i = 0; i < quantity; i++)
            {
                // First iteration is a special-case.. since its always 0..
                if (i == 0)
                    result[i] = 0;
                // Second and third iteration is also a "special case".. since they are always 1..
                else if (i <= 2)
                    result[i] = 1;
                else
                    result[i] = result[i - 1] + result[i - 2];

            }

            return result;
        }
    }
}
