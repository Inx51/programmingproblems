﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CombinedArrays.Presentation.Console;

namespace CombinedArrays.Test
{
    [TestClass]
    public class CombinedArraysTest
    {

        // Simple test for our combine arrays method..
        [TestMethod]
        public void Can_combine_arrays_with_solution_one()
        {
            // Arrange
            var arr1 = new char[] { 'a', 'b', 'c' };
            var arr2 = new int[] { 1, 2, 3 };

            // Act
            var combinedArray = Program.CombineArrays(arr1, arr2);

            // Assert
            var result = string.Join(", ", combinedArray);
            Assert.AreEqual("a, 1, b, 2, c, 3", result);
        }
    }
}
