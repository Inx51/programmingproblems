﻿using General;
using System;
using System.Linq;

namespace ThreeFunctionsSum.Presentation.Console
{
    // In this simple program we use four different ways to sum up an array of integers
    // Im fully aware of the usage of "UsingXXX" in a function-name isn't the correct way to name methods..
    // however.. just for the sake of "readability" and for clarity I decided to name the functions this way.


    public class Program
    {
        /// Out input array.. could most have been provided by using Console.ReadKey/ReadLine..
        /// but lets keep it simple..
        static int[] _numbers = new[] { 2, 3, 5 };

        static void Main(string[] args)
        {
            Logger.WriteLine("Method 1 (Using a for loop)");
            var sum = CalcSumUsingFor(_numbers);
            Logger.WriteLine(FormatSumMessage(sum));
            Logger.Separator();


            Logger.WriteLine("Method 2 (Using a while loop)");
            sum = CalcSumUsingWhile(_numbers);
            Logger.WriteLine(FormatSumMessage(sum));
            Logger.Separator();

            Logger.WriteLine("Method 3 (Using recursion)");
            // Lets pass in the startvalue and startindex as parameters
            // since we don't want to clutter our object/class-scope. (and besides..
            // its "more" re-usable when its "state-less" in this case..
            sum = CalcSumUsingRecursion(_numbers, 0, 0);
            Logger.WriteLine(FormatSumMessage(sum));
            Logger.Separator();

            Logger.WriteLine("Method 4 (Using LINQ and Sum)");
            sum = CalcSumUsingLinq(_numbers);
            Logger.WriteLine(FormatSumMessage(sum));
            Logger.Separator();

            // Lets just prevent us from exiting..
            System.Console.ReadKey();

        }

        public static int CalcSumUsingFor(int[] numbers)
        {
            int sum = 0;
            for (var i = 0; i < numbers.Length; i++)
            {
                sum += numbers[i];
            }

            return sum;
        }

        public static int CalcSumUsingWhile(int[] numbers)
        {
            int sum = 0;

            int iterations = 0;
            while (iterations < numbers.Length)
            {
                sum += numbers[iterations];
                iterations++;
            }

            return sum;
        }

        public static int CalcSumUsingRecursion(int[] numbers, int startsum, int startindex)
        {
            // Why would someone ever ever ever do this?.. its plain stupid..
            // the "readability" is extremely poor..

            if (startindex >= numbers.Length)
                return startsum;
            else
            {
                var value = numbers[startindex];
                var newsum = startsum + value;
                startindex++;
                var result = CalcSumUsingRecursion(numbers, newsum, startindex);
                return result;
            }
        }

        public static int CalcSumUsingLinq(int[] numbers)
        {
            // Most likely the easiest way of doing it... 
            // and probably the fastest and most efficient also..
            return numbers.Sum();
        }

        private static string FormatSumMessage(int sum)
        {
            return string.Format("Sum: {0}", sum);
        }
    }
}
