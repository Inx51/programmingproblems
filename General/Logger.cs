﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General
{
    /// <remark>
    /// We could implement this using an interface
    /// and then be able to use our preferred IoC to 
    /// for a more flexible logging solution
    /// if we had multiple channels to send out logs to..
    /// or to simply switch between channels.. but still
    /// using the same ILogger-interface..
    /// however.. we really don't need to do that here..
    /// since we will only log to our console applications.
    /// ..
    /// However.. at least we can simply switch to a different channel
    /// by altering this class (change Console.Write to a different channel
    /// that accepts a string as argument.. or hook the messages into the EventViewer/EventLogger
    /// for instance..
    /// </remark>
    public static class Logger
    {
        public static void WriteLine(string msg)
        {
            Console.WriteLine(msg);

            //EventLog.WriteEntry("SomeSourcename", msg, EventLogEntryType.Warning);
        }

        public static void Write(string msg)
        {
            Console.Write(msg);
        }

        // Simple way to create a separator when needed.
        public static void Separator()
        {
            Console.WriteLine(Environment.NewLine);
        }
    }
}
