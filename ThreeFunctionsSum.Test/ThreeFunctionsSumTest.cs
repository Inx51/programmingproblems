﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ThreeFunctionsSum.Presentation.Console;

namespace ThreeFunctionsSum.Test
{
    [TestClass]
    public class ThreeFunctionsSumTest
    {
        [TestMethod]
        public void Can_calc_sum_using_for_loop()
        {
            // Arrange
            var values = new int[] { 3, 2, 5 };

            // Act
            var result = Program.CalcSumUsingFor(values);

            // Assert
            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void Can_calc_sum_using_while_loop()
        {
            // Arrange
            var values = new int[] { 3, 2, 5 };

            // Act
            var result = Program.CalcSumUsingWhile(values);

            // Assert
            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void Can_calc_sum_using_for_recursion()
        {
            // Arrange
            var values = new int[] { 3, 2, 5 };

            // Act
            var result = Program.CalcSumUsingRecursion(values,0,0);

            // Assert
            Assert.AreEqual(10, result);
        }

        [TestMethod]
        public void Can_calc_sum_using_linq()
        {
            // Arrange
            var values = new int[] { 3, 2, 5 };

            // Act
            var result = Program.CalcSumUsingLinq(values);

            // Assert
            Assert.AreEqual(10, result);
        }
    }
}
