﻿using General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CombinedArrays.Presentation.Console
{
    // In this program we try to combine two arrays with equal depth into one array.
    // but with the twist of mixing them together in "order".


    public class Program
    {
        static char[] _chars = new[] { 'a', 'b', 'c' };
        static int[] _numbers = new[] { 1, 2, 3 };

        static void Main(string[] args)
        {
            // Lets just make sure that the program is setup correctly.
            Validate(_chars, _numbers);

            // Yeah.. poor naming of methods.. I know..
            // but as mentioned in previous problems, this is to be able to divide multiple solutions
            // to our problems in a readable way.. and usually we would not have to do this since we only solve
            // a problem once with the most efficient and reader-friendly solution (since we don't want our fellow developers to spend hours figuring out what our
            // method is doing).
            Logger.WriteLine("Solution 1");
            var result = CombineArrays(_chars, _numbers);
            PrintSolution(result);
            Logger.Separator();

            // Lets just prevent us from exiting..
            System.Console.ReadKey();
        }

        public static string[] CombineArrays(char[] chars, int[] numbers)
        {
            // Yeah yeah.. we could sum chars and numbers length.. but since we already have validate their length
            // we will just assume they have the same depth since the entire "problem" we are trying to solve rely on it..
            string[] targetArr = new string[chars.Length * 2];

            int counter = 0;
            // Create a temporary counter.

            // Lets set the target-array values.
            while (counter < targetArr.Length)
            {
                if (counter % 2 == 0)
                    targetArr[counter] = chars[counter / 2].ToString();
                else
                    targetArr[counter] = numbers[counter / 2].ToString();

                counter++;
            }

            return targetArr;
        }

        private static void PrintSolution(string[] result)
        {
            foreach (var value in result)
            {
                Logger.Write(value);
            }
        }

        private static void Validate(char[] chars, int[] numbers)
        {
            if(chars.Length != numbers.Length)
                throw new Exception("The arrays needs to have an equal number of indexes/depth");
        }
    }
}
