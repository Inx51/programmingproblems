﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargestPossibleNumber.Core.Extensions
{
    // Lets create extension methods for the following methods..
    // since they are quite common operations for the give type.

    public static class IntArrayExtensions
    {
        // Lets get the largest number in the array.
        /// <summary>
        /// Get the largest number in the array.
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static int GetLargestNumber(this int[] arr)
        {
            int temporary = int.MinValue;
            for (var i = 0; i < arr.Length; i++)
            {
                if (arr[i] > temporary)
                {
                    temporary = arr[i];
                }
            }

            return temporary;
        }

        // Lets get the smallest number in the array.
        /// <summary>
        /// Get the smallest number in the array.
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static int GetSmallestNumber(this int[] arr)
        {
            int temporary = int.MaxValue;
            for (var i = 0; i < arr.Length; i++)
            {
                if (arr[i] < temporary)
                {
                    temporary = arr[i];
                }
            }

            return temporary;
        }

        // Lets order by desc
        /// <summary>
        /// Orders the array in descending order.
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static int[] Descending(this int[] arr)
        {
            var result = new int[arr.Length];

            for (var i = arr.Length - 1; i > -1; i--)
            {
                result[arr.Length - i - 1] = arr[i];
            }

            return result;
        }
    }
}
