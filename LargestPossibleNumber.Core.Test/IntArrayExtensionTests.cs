﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LargestPossibleNumber.Core.Extensions;

namespace LargestPossibleNumber.Core.Test
{
    [TestClass]
    public class IntArrayExtensionTests
    {
        [TestMethod]
        public void Can_get_largest_number()
        {
            // Arrange
            var numbers = new int[]{ 1,2,3 };

            // Act
            var largest = numbers.GetLargestNumber();

            // Assert
            Assert.AreEqual(largest, 3);
        }

        [TestMethod]
        public void Can_get_smallest_number()
        {
            // Arrange
            var numbers = new int[] { 1, 2, 3 };

            // Act
            var smallest = numbers.GetSmallestNumber();

            // Assert
            Assert.AreEqual(smallest, 1);
        }

        [TestMethod]
        public void Can_order_array_descending()
        {
            // Arrange
            var numbers = new int[] { 1, 2, 3 };

            // Act
            var descArr = numbers.Descending();

            // Assert
            if (!descArr.SequenceEqual(new int[] { 3, 2, 1 }))
                Assert.Fail();
        }
    }
}
