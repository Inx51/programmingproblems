Programming Problems
==================

The following programming problems are based up on the following article:
http://www.shiftedup.com/2015/05/07/five-programming-problems-every-software-engineer-should-be-able-to-solve-in-less-than-1-hour

They are all simple logic tests, and should not be considered as actual or final implementations.. they are all just for the sake of logic.

I have tried to write the solutions to the problems as language independet and framework independent as possible. With an exception of the tests!