﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LargestPossibleNumber.Presentation.Console;

namespace LargestPossibleNumber.Presentation.Test
{
    [TestClass]
    public class LargestPossibleNumberTest
    {
        [TestMethod]
        public void Can_get_largest_combined_number()
        {
            // Assign
            var numbers = new int[] { 10, 3, 900 };

            // Act
            var largestcombinednumber = Program.GetLargestNumberCombination(numbers);

            // Assert
            Assert.AreEqual(largestcombinednumber, 900103);
        }
    }
}
