﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FirstXFibonacciNumbers.Presentation.Console;

namespace FirstXFibonacciNumbers.Test
{
    [TestClass]
    public class FirstXFibonacciNumbersTest
    {
        [TestMethod]
        public void Can_calc_fibonaccis()
        {
            // Assign
            var expectedFirstEightResults = new ulong[] { 0, 1, 1, 2, 3, 5, 8, 13 };
            ulong expectedHundreadResult = 16008811023750101250;

            // Act
            var fibonaccis = Program.GetFibonaccis(100);
            var firstEigth = fibonaccis.Take(8).ToArray();
            var lastHundread = fibonaccis[99];

            // Assert
            if (!expectedFirstEightResults.SequenceEqual(firstEigth))
                Assert.Fail();

            // Lets verify that we don't end up with negative numbers.. (overflow)
            Assert.AreEqual(expectedHundreadResult, lastHundread);
        }
    }
}
