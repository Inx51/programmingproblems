﻿using General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LargestPossibleNumber.Core.Extensions;

namespace LargestPossibleNumber.Presentation.Console
{
    // Combines a few integers in an array into the largest possible number.

        
    public class Program
    {
        // Our numbers..
        private static int[] _numbers = new []{1,12,2,1,40,900,13};

        static void Main(string[] args)
        {
            var largetsnumber = GetLargestNumberCombination(_numbers);
            Logger.WriteLine("The largest number is: " + largetsnumber);

            // Lets just prevent us from exiting..
            System.Console.ReadKey();
        }

        // Lets convert the array to a single value (a long in this case)
        public static long GetLargestNumberCombination(int[] numbers)
        {
            var smallest = numbers.GetSmallestNumber();
            var largest = numbers.GetLargestNumber();

            //Special case..
            var numbersAsc = GetAllNumbersAsc(smallest, largest, numbers);

            var numbersDesc = numbersAsc.Descending();

            string temp_number = "";
            for (var i = 0; i < numbersDesc.Length; i++)
            {
                temp_number += numbersDesc[i].ToString();
            }
            return long.Parse(temp_number);
        }

        // Lets get the numbers in between in order.
        private static int[] GetAllNumbersAsc(int smallest, int largest, int[] numbers)
        {
            var temp = new int[numbers.Length];
            var counter = 0;
            // Lets just calc within the appropriate range, for the sake of "optimization".. (if you for some reason would call this optimization..
            // once again.. google this problem and you will most likely find one-liners that are more optimized.. so to quote one famous "entrepreneur"..
            // "Good artists copy, great artists steal" - Steve.J
            for(var i = smallest; i < largest+1; i++)
            {
                foreach(var num in numbers)
                {
                    if(num == i)
                    {
                        temp[counter] = num;
                        counter++;
                    }
                }
                if (counter == temp.Length)
                    break;
            }

            return temp;

        }
    }
}
